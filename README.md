# test

> A Vue.js project

## Build Setup

``` bash
# install dependencies
npm install

# serve with hot reload at localhost:8080
npm run dev

# build for production with minification
npm run build
```

# Laravel installation and configuration for this frontend:

## 1. Laravel installation :
```$ laravel new project_name ```
```$ cd project_name ```
```$ composer install ```

## 2. Post installation basic configuration :
Create config in .ENV file (database settings), then: 

```$ php artisan make:auth ```
```$ php artisan migrate ```

! If You got problems here (with Laravel version 5.4 +) just make small mod in file app/Providers/AppServiceProvider.php :

add: ```use Illuminate\Support\Facades\Schema;```
and:
```
    public function boot()
    {
        Schema::defaultStringLength(191);
    }
```

## 3. Serve the website and create first user (register)

```$ php artisan serve ```

## 4. Install passport package:

according to : https://laravel.com/docs/5.4/passport

```$ composer require laravel/passport ```

Next, register the Passport service provider in the providers array of your config/app.php configuration file (in section: "Package Service Providers" in array "providers" ):

```Laravel\Passport\PassportServiceProvider::class,```

next :
```$ php artisan migrate```
```$ php artisan passport:install```
Now just copy credentials for Client ID : 2 and paste it in Vue project in file :
src/components/user/UserLogin.vue as client_secret variable

## 5. CORS package installation

Description based on : https://github.com/barryvdh/laravel-cors

```$ composer require barryvdh/laravel-cors```
In config/app.php file add (in section: "Package Service Providers" in array "providers" ):
```Barryvdh\Cors\ServiceProvider::class,```
next :
```$ php artisan vendor:publish --provider="Barryvdh\Cors\ServiceProvider"```

## 6. Finishing:
```
# in file app/Http/Kernel.php add to 'protected $middleware' array :
\Barryvdh\Cors\HandleCors::class
```

```
# in file /Http/Middleware/VerifyCsrfToken.php add to 'protected $except' array :
'api/*'
```

```
# in file /Providers/AuthServiceProvider.php add:
use Laravel\Passport\Passport;
use Carbon\Carbon;

    public function boot()
    {
        $this->registerPolicies();
        Passport::routes();
        Passport::tokensExpireIn(Carbon::now()->addDays(15));
        Passport::refreshTokensExpireIn(Carbon::now()->addDays(30));
    }
```

Now just create in routes/api.php this lines :

```
Route::group(['middleware' => 'auth:api'], function(){
    Route::get('/testuser', function(){
        return response()->json( [123,234,345] );
    });
}); 
```
and in file src/components/user/UserPanel.vue, change api route to :
```this.$http.get('api/testuser')``` and try login and if You see array in /panel site in Vue, its done :)

## 7 - Fix :
in file app/User.php ( this is user model file ) add :

```
    # use section:
        use Laravel\Passport\HasApiTokens;

    in class add: 
        use HasApiTokens, Notifiable;
```

AND in config / auth.php change :
    guards -> api -> driver to :

    ```
        'api' => [
            'driver' => 'passport',
            'provider' => 'users',
        ],
    ```

