import Vue from 'vue'
import BootstrapVue from 'bootstrap-vue'
import VueRouter from 'vue-router'
import VueResource from 'vue-resource'
import App from './App.vue'
import { routes } from './routes'
import Auth from './auth'
import Myconf from './myconf'

Vue.use(BootstrapVue);
Vue.use(VueRouter);
Vue.use(VueResource);
Vue.use(Auth);
Vue.use(Myconf);



Vue.http.options.root = Vue.myconf.laravel.host;
Vue.http.headers.common['Authorization'] = 'Bearer ' + Vue.auth.getToken();

// Vue.http.interceptors.push((request, next) => {
//   request.headers.set('Authorization', 'Bearer ' + Vue.auth.getToken())
//   next()
// })

Vue.http.interceptors.push((request, next) => {
  const token = Vue.auth.getToken();
  const hasAuthHeader = request.headers.has('Authorization')
  if (token && !hasAuthHeader) {
    request.headers.set('Authorization', 'Bearer ' + Vue.auth.getToken())
  }
  next((response) => {
    if (response.status === 401 && response.data.error === 'invalid_token') {
      request.headers.set('Authorization', 'Bearer ' + Vue.auth.getToken());
      return
    }
  })
})

const router = new VueRouter({
  mode: 'history',
  routes
});

router.beforeEach(
  (to, from, next) => {
    if (to.matched.some(record => record.meta.forVisitors)) {
      if (Vue.auth.isAuthenticated()) {
        next({
          path: '/panel'
        });
      } else {
        next();
      }
    } else if (to.matched.some(record => record.meta.forAuth)) {
      if (!Vue.auth.isAuthenticated()) {
        next({
          path: '/login'
        });
      } else {
        next();
      }
    } else {
      next();
    }
  }
)

new Vue({
  el: '#app',
  router,
  render: h => h(App)
})

