export default function (Vue) {
    Vue.myconf = {
        auth: { // set auth data from oauth/laravel
            client_id: 00,
            client_secret: '',
            grant_type: 'password', //default
        },
        laravel:{
            host: ''
        }
    }

    Object.defineProperties(Vue.prototype, {
        $myconf: {
            get() {
                return Vue.myconf;
            }
        }
    });
}