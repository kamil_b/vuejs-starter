import Main from './components/Main.vue'
import UserLogin from './components/user/UserLogin.vue'
import UserRegister from './components/user/UserRegister.vue'
import UserPanel from './components/user/UserPanel.vue'

export const routes = [
    { path: '/', component: Main },
    { path: '/register', component: UserRegister, meta: { forVisitors: true } },
    { path: '/login', component: UserLogin, meta: { forVisitors: true } },
    { path: '/panel', component: UserPanel, meta: { forAuth: true } },
]